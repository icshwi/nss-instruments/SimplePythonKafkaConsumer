#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

from BaseDataParser import BaseDataParser
import codecs
from f142 import LogData

class f142_parser(BaseDataParser):
    def __init__(self):
        param_list = ["File id", "Source name", "Timestamp (ns)", "Value type", "Value"]
        super(f142_parser, self).__init__(extra_params = param_list)
    
    def GetAttrValueStr(self, attr):
        
        return "n/a"

    def my_fb_parser(self, data):
        logData = LogData.LogData.GetRootAsLogData(data, 0)
        ret_dict = {}
        ret_dict["File id"] = "n/a"
        ret_dict["Source name"] = logData.SourceName()
        ret_dict["Timestamp (ns)"] = logData.Timestamp()
        ret_dict["Value type"] = logData.ValueType()
        ret_dict["Value"] = logData.Value()
        return ret_dict
